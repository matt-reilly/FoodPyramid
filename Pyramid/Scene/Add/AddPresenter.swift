//
// Created by Matt on 7/30/17.
// Copyright (c) 2017 Pyramid. All rights reserved.
//

import Foundation



class AddPresenter {

    var view: AddViewing?
    let repo: FoodRepo

    init(repo: FoodRepo) {
        self.repo = repo
    }

    func fetch() {
        view?.set(categories: FoodCategory.all)
    }

    func save(_ text: String, category: FoodCategory) {
        repo.add(foodItem: text, to: category)
    }
}