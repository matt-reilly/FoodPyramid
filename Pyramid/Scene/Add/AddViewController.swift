//
//  FirstViewController.swift
//  Pyramid
//
//  Created by Matt on 7/30/17.
//  Copyright (c) 2017 Pyramid. All rights reserved.
//

import UIKit
import SnapKit

protocol AddViewing: class {
    func set(categories: [FoodCategory])
}

class AddViewController: UIViewController, AddViewing, UIPickerViewDataSource, UIPickerViewDelegate {

    let presenter: AddPresenter
    let router: Router

    private var categories: [FoodCategory] = []
    var selectedCategory: FoodCategory?

    lazy var addButton: UIButton = {
        let button = UIButton()

        button.backgroundColor = .darkGray
        button.setTitleColor(.white, for: .normal)
        button.setTitle("Save", for: .normal)
        button.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)
        self.view.addSubview(button)
        return button
    }()

    lazy var textField: UITextField = {
        let field = UITextField()
        field.placeholder = "Item name"
        self.view.addSubview(field)
        field.becomeFirstResponder()
        return field
    }()


    lazy var pickerView: UITextField = {
        let field = UITextField()
        field.placeholder = "Category"
        self.view.addSubview(field)
        field.inputView = self.categoryPicker
        field.becomeFirstResponder()
        return field
    }()


    lazy var categoryPicker: UIPickerView = {
        let field = UIPickerView()
        field.delegate = self
        field.dataSource = self
        return field
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.view = self
        presenter.fetch()

        self.view.backgroundColor = .white

        setUpConstraints()
    }

    func saveTapped() {
        if let title = textField.text, let category = selectedCategory {
            presenter.save(title, category: category)
            self.dismiss(animated: true)
        }
    }

    init(router: Router, presenter: AddPresenter) {
        self.router = router
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)

        self.title = "Add Item"

    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row].rawValue.capitalized
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerView.text = categories[row].rawValue.capitalized
        selectedCategory = categories[row]
        self.view.endEditing(true)
    }

    func set(categories: [FoodCategory]) {
        self.categories = categories
        categoryPicker.reloadAllComponents()
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }

    func setUpConstraints() {
        addButton.snp.makeConstraints { make in
            make.leading.bottom.trailing.equalTo(self.view)
            make.height.equalTo(50)
        }

        textField.snp.makeConstraints { make in
            make.center.equalTo(self.view)
            make.width.equalTo(140)
            make.height.equalTo(40)

        }

        pickerView.snp.makeConstraints { make in
            make.top.equalTo(self.textField.snp.bottom).offset(40)
            make.width.equalTo(140)
            make.centerX.equalTo(self.view)
            make.height.equalTo(40)

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}