//
// Created by Matt on 7/30/17.
// Copyright (c) 2017 Pyramid. All rights reserved.
//

import Foundation



class HomePresenter {

    var view: HomeViewing?
    let repo: FoodRepo

    init(repo: FoodRepo) {

        self.repo = repo
    }

    func fetch() {
        view?.set(items: repo.getFoodItems())
    }

    func update() {
        view?.update(items: repo.getFoodItems())
    }
}