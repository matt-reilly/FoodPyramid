//
//  FirstViewController.swift
//  Pyramid
//
//  Created by Matt on 7/30/17.
//  Copyright (c) 2017 Pyramid. All rights reserved.
//

import UIKit
import SnapKit

protocol HomeViewing: class {
    func set(items: [Category])
    func update(items: [Category])
}

class HomeViewController: UIViewController, HomeViewing, PyramidDelegate {

    lazy var pyramidView: PyramidView = {
        let pyramid = PyramidView()
        pyramid.delegate = self
        self.view.addSubview(pyramid)
        return pyramid
    }()

    let presenter: HomePresenter
    let router: Router

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white

        setUpConstraints()

        self.title = "Food Pyramid"
    }

    init(router: Router, presenter: HomePresenter) {
        self.router = router
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        presenter.view = self
        presenter.fetch()
        setUpConstraints()


        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
    }

    func addTapped() {
        router.push(to: .add, from: self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        presenter.update()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }

    func set(items: [Category]) {
        pyramidView.set(items: items)
    }

    func update(items: [Category]) {
        pyramidView.update(with: items)
    }

    func didSelect(category: Category) {
        router.go(to: .detail(category: category))
    }

    func setUpConstraints() {
        pyramidView.snp.makeConstraints { make in
            make.edges.equalTo(self.view)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
