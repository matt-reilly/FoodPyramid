//
//  FirstViewController.swift
//  Pyramid
//
//  Created by Matt on 7/30/17.
//  Copyright (c) 2017 Pyramid. All rights reserved.
//

import UIKit
import SnapKit

protocol DetailViewing: class {

}

class DetailViewController: UIViewController, DetailViewing {

    let presenter: DetailPresenter
    let router: Router
    let dataSource: DetailDataSource

    lazy var tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        table.register(CategoryCell.self, forCellReuseIdentifier: CategoryCell.identifier)
        table.allowsSelection = false
        table.rowHeight = 75
        self.view.addSubview(table)
        return table

    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white

        setUpConstraints()
    }

    init(router: Router, category: Category, presenter: DetailPresenter) {
        self.router = router
        self.presenter = presenter
        dataSource = DetailDataSource(category: category)

        super.init(nibName: nil, bundle: nil)

        self.view.backgroundColor = category.color
        self.title = category.category.rawValue.capitalized
        presenter.view = self
        setUpConstraints()

        tableView.delegate = dataSource
        tableView.dataSource = dataSource
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }

    func setUpConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalTo(self.view)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

class DetailDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {

    private let category: Category

    init(category: Category) {
        self.category = category
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return category.items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCell.identifier, for: indexPath) as! CategoryCell
        cell.setUp(with: category.items[indexPath.row])
        return cell
    }

}


class CategoryCell: UITableViewCell {

    static let identifier = "CategoryCell"

    var item: Item?
    var itemCount: Int = 0

    lazy var stepper: UIStepper = {
        let stepper = UIStepper()
        self.contentView.addSubview(stepper)
        stepper.addTarget(self, action: #selector(stepperValueChanged(sender:)), for: .valueChanged)

        return stepper
    }()

    lazy var title: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20)
        self.contentView.addSubview(label)

        return label
    }()

    lazy var count: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 22)
        label.textAlignment = .center
        self.contentView.addSubview(label)

        return label
    }()

    lazy var containerView: UIView = {
        let view = UIView()
        self.contentView.addSubview(view)
        return view
    }()

    func setUp(with item: Item) {
        self.item = item

        stepper.value = Double(item.servings)
        updateValues()
        setupConstraints()
    }

    func updateValues() {
        guard let itemTitle = item?.title, let itemServings = item?.servings else { return }
        self.title.text = itemTitle
        self.count.text = String(itemServings)
    }

    func stepperValueChanged(sender: UIStepper) {
        item?.servings = Int(sender.value)
        updateValues()
    }

    private func setupConstraints() {
        count.snp.makeConstraints { make in
            make.top.bottom.equalTo(self.contentView)
            make.leading.equalTo(self.contentView).inset(20)
            make.trailing.equalTo(self.title.snp.leading)
            make.width.equalTo(40)
        }

        title.snp.makeConstraints { make in
            make.leading.equalTo(self.count.snp.trailing)
            make.top.bottom.equalTo(self.contentView)
            make.trailing.equalTo(self.stepper.snp.leading)
        }

        stepper.snp.makeConstraints { make in
            make.centerY.equalTo(self.contentView)
            make.trailing.equalTo(self.contentView).inset(20)
            make.leading.equalTo(self.title.snp.trailing)
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    override class var requiresConstraintBasedLayout: Bool {
        return true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}