//
// Created by Matt on 7/30/17.
// Copyright (c) 2017 Pyramid. All rights reserved.
//

import Foundation
import UIKit
import DynamicColor

protocol FoodProvider {
    func getFoodItems() -> [Category]
}

class FoodRepo: FoodProvider {

    var dairy = FoodGroup(category: .dairy,
            items: [FoodItem(category: .dairy, title: "Milk", servings: 11)],
            color: UIColor(hexString: "#3498db"))
    var meat = FoodGroup(category: .meat,
            items: [FoodItem(category: .meat, title: "Beef", servings: 4),
                    FoodItem(category: .meat, title: "Chicken", servings: 6)],
            color: UIColor(hexString: "#e74c3c"))
    var veggies = FoodGroup(category: .vegetables,
            items: [FoodItem(category: .vegetables, title: "Broccoli", servings: 5),
                    FoodItem(category: .vegetables, title: "Peas", servings: 3)],
            color: UIColor(hexString: "#2ecc71"))
    var fruits = FoodGroup(category: .fruit,
            items: [FoodItem(category: .fruit, title: "Orange", servings: 4)],
            color: UIColor(hexString: "#8e44ad"))
    var starches = FoodGroup(category: .starches,
            items: [FoodItem(category: .starches, title: "Bread", servings: 14)],
            color: UIColor(hexString: "#f1c40f"))

    var items: [FoodCategory: FoodGroup] {
        return [FoodCategory.starches: self.starches,
                FoodCategory.fruit: self.fruits,
                FoodCategory.vegetables: self.veggies,
                FoodCategory.meat: self.meat,
                FoodCategory.dairy: self.dairy]
    }

    func getFoodItems() -> [Category] {

        return [dairy, meat, veggies, fruits, starches].sorted { (group: FoodGroup, group1: FoodGroup) -> Bool in
            return group.amount < group1.amount
        }
    }

    func add(foodItem: String, to category: FoodCategory) {
        if let group: FoodGroup = items[category] {
            group.items.append(FoodItem(category: category, title: foodItem, servings: 1))
        }
    }
}

class FoodGroup: Category {
    var category: FoodCategory
    var items: [Item] = []
    private(set) var color: UIKit.UIColor? = .gray

    init(category: FoodCategory, items: [FoodItem], color: UIColor) {
        print("init")
        print(category)
        self.category = category
        self.items = items
        self.color = color
    }

    var amount: Int {
        return Int(self.items.reduce(0) {
            $0 + CGFloat($1.servings)
        })
    }
}

class FoodItem: Item {
    var category: FoodCategory
    var title: String = ""
    var servings = 0

    init(category: FoodCategory, title: String, servings: Int) {
        self.category = category
        self.title = title
        self.servings = servings
    }
}