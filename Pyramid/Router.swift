//
// Created by Matt on 7/30/17.
// Copyright (c) 2017 Pyramid. All rights reserved.
//

import Foundation
import UIKit

enum Scene {
    case detail(category: Category)
    case add

    func controller(router: Router) -> UIViewController {

        switch self {
            case let .detail(category):
                let detail = DetailViewController(router: router, category: category, presenter: DetailPresenter(repo: router.foodRepo))
                return detail
            case let .add:
                let detail = UINavigationController(rootViewController: AddViewController(router: router, presenter: AddPresenter(repo: router.foodRepo)))
                return detail
        }
    }

}

class Router {

    weak var root: UIViewController?

    init() {

    }

    lazy var foodRepo: FoodRepo = {
        return FoodRepo()
    }()

    func go(to scene: Scene) {
        root?.navigationController?.pushViewController(scene.controller(router: self), animated: true)
    }

    func push(to scene: Scene, from: UIViewController) {
        from.present(scene.controller(router: self), animated: true)
    }
}