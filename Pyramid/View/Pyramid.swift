//
// Created by Matt on 7/30/17.
// Copyright (c) 2017 Pyramid. All rights reserved.
//

import Foundation
import UIKit


protocol Category: class {
    var category: FoodCategory { get set }
    var items: [Item] { get set }
    var color: UIColor? { get }
    var amount: Int { get }
}

protocol Item: class {
    var category: FoodCategory { get set }
    var title: String { get set }
    var servings: Int { get set }
}

enum FoodCategory: String {
    static var all: [FoodCategory] {
        return [.dairy, .vegetables, .meat, .fruit, .starches]
    }
    case dairy
    case vegetables
    case meat
    case fruit
    case starches
}

protocol PyramidDelegate {
    func didSelect(category: Category)
}

class PyramidView: UIView {

    var amounts: [Category] = []
    var bars: [PyramidBar] = []

    var delegate: PyramidDelegate?

    var barHeight = 65
    var barItemStep = 20


    lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = UILayoutConstraintAxis.vertical
        stackView.distribution = UIStackViewDistribution.fillEqually
        stackView.alignment = UIStackViewAlignment.leading
        stackView.spacing = 16.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(stackView)

        stackView.snp.makeConstraints { make in
            make.center.equalTo(self).priority(.high)
            make.width.height.equalTo(350).priority(.high)

        }
        stackView.layoutIfNeeded()
        return stackView

    }()

    func updateIfNeeded() {

        for (index, bar) in bars.enumerated() {
            guard let category = bar.item else { return }
            bar.update()

            UIView.animate(withDuration: 0.6,
                    delay: 0.24 + (0.1 * Double(index)),
                    usingSpringWithDamping: 0.8,
                    initialSpringVelocity: 1.4,
                    options: .allowUserInteraction,
                    animations: {

                        let width: CGFloat = CGFloat(category.amount * self.barItemStep) + 25

                        if category.amount == 0 {
                            bar.alpha = 0.6
                        } else {
                            bar.alpha = 1.0
                        }
                        print(width)
                        bar.snp.updateConstraints { make in
                            make.width.equalTo(width)
                        }

                        self.stackView.layoutIfNeeded()

                    })


        }
    }

    init() {
        super.init(frame: .zero)
    }

    func update(with: [Category]) {
        amounts = with
        updateIfNeeded()
    }

    func set(items: [Category]) {

        amounts = items

        for (index, item) in amounts.enumerated() {

            let bar = PyramidBar()
            bar.backgroundColor = item.color


            bar.snp.makeConstraints { make in
                make.height.equalTo(self.barHeight)
                make.width.equalTo(1)
            }

            bar.item = item

            stackView.addArrangedSubview(bar)

            bar.alpha = 0.0
            bars.append(bar)
            bar.tag = index
            bar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PyramidView.handleTap(gestureRecognizer:))))

            stackView.setNeedsLayout()
            stackView.layoutIfNeeded()

        }

        updateIfNeeded()
    }

    func handleTap(gestureRecognizer: UIGestureRecognizer) {

        guard let index = gestureRecognizer.view?.tag, let color = amounts[index].color else {
            return
        }

        let bar = self.bars[index]

        bar.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
        UIView.animate(withDuration: 0.5,
                delay: 0,
                usingSpringWithDamping: 0.8,
                initialSpringVelocity: 1.4,
                options: .allowUserInteraction,
                animations: {
                    bar.transform = .identity
                },
                completion: { complete in
                    guard let category = self.bars[index].item else { return }
                    self.delegate?.didSelect(category: category)
                    let view = self.bars[index]
                })
    }

    func add(item: Item, to: Category) {

    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}