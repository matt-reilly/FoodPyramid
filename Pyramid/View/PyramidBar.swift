//
// Created by Matt on 7/31/17.
// Copyright (c) 2017 Pyramid. All rights reserved.
//

import Foundation
import UIKit

class PyramidBar: UIView {

    var item: Category? {
        didSet {
            update()
        }
    }

    lazy var label: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15)
        label.alpha = 0.6
        label.textColor = .white
        self.addSubview(label)
        return label
    }()

    lazy var count: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 22)
        label.alpha = 0.3
        label.textAlignment = .right
        label.textColor = .white
        self.addSubview(label)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    func update() {
        if let cat = item {
            label.text = cat.category.rawValue.capitalized
            count.text = String(cat.amount)
        }
    }

    func configure() {
        label.snp.makeConstraints { make in
            make.height.equalTo(self.frame.height)
            make.width.equalTo(150)
            make.leading.top.bottom.equalTo(self).inset(8)
        }

        count.snp.makeConstraints { make in
            make.height.equalTo(self.frame.height)
            make.width.equalTo(50)
            make.trailing.top.bottom.equalTo(self).inset(10)
        }

        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
    }
}